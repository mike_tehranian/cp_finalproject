import heapq

REMOVED = '<removed-pixel>'

class PriorityQueue():

    def __init__(self):
        self.queue = []
        self.entry_finder = {}

    def __contains__(self, pixel):
        return pixel in self.entry_finder

    def __eq__(self, other):
        return self == other

    def __str__(self):
        return 'PQ:[%s]' % (', '.join([str(i) for i in self.queue]))

    def enqueue(self, distance, pixel):
        if pixel in self.entry_finder:
            self.remove(pixel)
        entry = [distance, pixel]
        self.entry_finder[pixel] = entry
        heapq.heappush(self.queue, entry)

    def dequeue(self):
        while self.queue:
            distance, pixel = heapq.heappop(self.queue)
            if pixel is not REMOVED:
                del self.entry_finder[pixel]
                return distance, pixel
        raise KeyError('Dequeue on an empty priority queue')

    def remove(self, pixel):
        entry = self.entry_finder.pop(pixel)
        entry[-1] = REMOVED

    def clear(self):
        self.queue = []

    def size(self):
        return len(self.queue)

    def top(self):
        return self.queue[0]
