import argparse
import errno
import os

import cv2
import numpy as np

from glob import glob

from queue import PriorityQueue


KERNEL_SIZE = 3


def gradient_energy_scaled(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    sobel_x = cv2.Sobel(gray,cv2.CV_64F,1,0,ksize=KERNEL_SIZE)
    sobel_y = cv2.Sobel(gray,cv2.CV_64F,0,1,ksize=KERNEL_SIZE)

    gradient_magnitude = cv2.magnitude(sobel_x, sobel_y)

    scaled_image = cv2.convertScaleAbs(gradient_magnitude)

    return scaled_image


def gradient_energy_normalized(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    sobel_x = cv2.Sobel(gray,cv2.CV_64F,1,0,ksize=KERNEL_SIZE)
    sobel_y = cv2.Sobel(gray,cv2.CV_64F,0,1,ksize=KERNEL_SIZE)

    gradient_magnitude = cv2.magnitude(sobel_x, sobel_y)

    normalized_image = cv2.normalize(gradient_magnitude, alpha=0,beta=255,
            norm_type=cv2.NORM_MINMAX)

    return normalized_image


def histogram_of_gradients(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hog = cv2.HOGDescriptor()
    gradient_magnitudes, angles = hog.computeGradient(gray)
    hog_x = gradient_magnitudes[:,:,0]
    hog_y = gradient_magnitudes[:,:,1]

    gradient_magnitude = cv2.magnitude(hog_x, hog_y)

    scaled_image = cv2.convertScaleAbs(gradient_magnitude)
    return scaled_image
    # normalized_image = cv2.normalize(gradient_magnitude, alpha=0,beta=255,
            # norm_type=cv2.NORM_MINMAX)
    # return normalized_image


def find_seam_path(image, energy_map):
    """Find the lowest energy seam in the energy map"""
    """Implements the path finding using Dijkstra's algorithm"""
    num_rows, num_cols = image.shape[:2]

    # Seam length is the height of the image
    seam = np.zeros(num_rows, dtype=np.uint32)

    queue = PriorityQueue()
    distance = np.full((num_rows, num_cols), np.NAN)
    previous_pixel = {}

    # The entire top row are sources in the shortest path
    distance[0,:] = 0.
    for column, energy_value in enumerate(energy_map[0]):
        queue.enqueue(energy_value, (0, column))

    while True:
        energy, (current_row,current_col) = queue.dequeue()

        # Generate path once we reach the bottom
        if current_row == num_rows - 1:
            return create_path_to_source(previous_pixel, (current_row,current_col))

        # Find each of the neighbors of the current pixel
        neighbors = []
        # Pixel directly beneath
        neighbors.append((current_row+1, current_col))
        if current_col != 0:
            # Pixel to the bottom left
            neighbors.append((current_row+1, current_col-1))
        if current_col != num_cols - 1:
            # Pixel to the bottom right
            neighbors.append((current_row+1, current_col+1))

        # Update the distances for each of the neighbors
        for neighbor in neighbors:
            neighbor_row, neighbor_col = neighbor
            # Find the new distance to the pixel
            alt = distance[current_row,current_col] + energy_map[neighbor_row][neighbor_col]

            # If the pixel has not been previously visited
            # Or if the distance is less than previously seen
            if np.isnan(distance[neighbor_row][neighbor_col]) \
                or alt < distance[neighbor_row][neighbor_col]:
                distance[neighbor_row][neighbor_col] = alt
                previous_pixel[(neighbor_row, neighbor_col)] = (current_row,current_col)
                queue.enqueue(alt,(neighbor_row,neighbor_col))


def create_path_to_source(previous_pixel, end_pixel):
    path = []
    u = end_pixel
    while u is not None:
        path = [u] + path
        u = previous_pixel.get(u)
    return path


def remove_columns(image, image_energy, num_seams, energy_fn):
    """Remove vertical seams from an image based on the num_seams argument"""
    for i in range(num_seams):
        seam_path = find_seam_path(image, image_energy)
        image = remove_vertical_seam(image, seam_path)
        image_energy = energy_fn(image)

    return image


def remove_vertical_seam(image, seam_path):
    """Remove one vertical seam path from an image"""
    rows, cols = image.shape[:2]

    new_image = np.zeros(image.shape, dtype=np.uint8)
    new_image = np.delete(new_image, cols-1, axis=1)

    for row, col in seam_path:
        new_image[row,:,:] = np.delete(image[row], col, axis=0)

    return new_image


def add_columns(image, image_energy, num_seams, energy_fn):
    """Add vertical seams from an image based on the num_seams argument"""

    image_output = np.copy(image)
    for i in range(num_seams):
        seam_path = find_seam_path(image, image_energy)
        image = remove_vertical_seam(image, seam_path)
        image_output = add_vertical_seam(image_output, seam_path, i)
        image_energy = energy_fn(image)

    return image_output


def add_vertical_seam(image, seam_path, seam_offset):
    """Remove one vertical seam path from an image"""
    rows, cols, channels = image.shape[:3]

    seam_path = tuple((x,y+seam_offset) for x,y in seam_path)

    # Copy the image and add an extra column
    new_image = np.zeros((rows,cols+1,channels), dtype=np.uint8)
    new_image[:,:-1,:] = image

    for row in range(rows):
        seam_col = seam_path[row][1]

        new_image[row,seam_col+1:,:] = image[row,seam_col:,:]

        # Is our seam on the right edge?
        if seam_col == cols-1:
            left = image[row,seam_col-2:seam_col,:]
            new_image[row,seam_col,:] = np.mean(left,axis=0)

        # Is our seam on the left edge?
        elif seam_col == 0:
            right = image[row,seam_col:seam_col+2,:]
            new_image[row,seam_col,:] = np.mean(right,axis=0)

        else:
            # By convention, use the left neighbor for averaging
            left = image[row,seam_col-1:seam_col+1,:]
            new_image[row,seam_col,:] = np.mean(left,axis=0)

    return new_image


def main(input_image_file, output_rows, output_cols, energy_fn):
    """Image processing pipeline for seam carving rows and columns"""

    image = cv2.imread(input_image_file, cv2.IMREAD_UNCHANGED | cv2.IMREAD_COLOR)
    rows, cols = image.shape[:2]
    image_original = np.copy(image)

    if cols > output_cols:
        num_cols = cols - output_cols
        image_energy = energy_fn(image)
        image = remove_columns(image, image_energy, num_cols, energy_fn)
    if cols < output_cols:
        cols_diff = output_cols - cols
        image_energy = energy_fn(image)
        image = add_columns(image, image_energy, cols_diff, energy_fn)

    if rows > output_rows:
        rows_diff = rows - output_rows
        # Removing horizontal seams is simply the transpose of removing
        # Vertical Seams
        image_rotated = np.transpose(image, (1, 0, 2))
        image_energy = energy_fn(image_rotated)
        image_rotated = remove_columns(image_rotated, image_energy, rows_diff, energy_fn)
        image = np.transpose(image_rotated, (1, 0, 2))
    if rows < output_rows:
        rows_diff = output_rows - rows
        # Adding horizontal seams is simply the transpose of adding
        # Vertical Seams
        image_rotated = np.transpose(image, (1, 0, 2))
        image_energy = energy_fn(image_rotated)
        image_rotated = add_columns(image_rotated, image_energy, rows_diff, energy_fn)
        image = np.transpose(image_rotated, (1, 0, 2))

    cv2.imshow('Input', image_original)
    cv2.imshow('Output', image)
    cv2.waitKey()
    cv2.imwrite(os.path.join("output",
        '{}_seam_carve.png'.format("output")), image)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Image Seam Carving.')
    parser.add_argument(
        '-f', '--input', help="Input image file.")
    parser.add_argument(
        '-r', '--rows', type=int, help="The width of the new image.")
    parser.add_argument(
        '-c', '--columns', type=int, help="The height of the new image.")
    parser.add_argument(
        '-e,', '--energy_fn', choices=["gradient", "hog"],
        help="Select the type of the energy function")
    args = parser.parse_args()

    try:
        output_dir = os.path.join("output")
        not_empty = not all([os.path.isdir(x) for x in
                             glob(os.path.join(output_dir, "*.*"))])
        if not_empty:
            raise RuntimeError("Output directory is not empty - aborting.")
        os.makedirs(output_dir)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

    if args.energy_fn == "gradient":
        energy_fn = gradient_energy_scaled
    else:
        energy_fn = histogram_of_gradients

    main(args.input, args.rows, args.columns, energy_fn)
